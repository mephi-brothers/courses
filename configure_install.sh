#!/bin/bash
# install apps, packets.
sudo apt-get install -y tree redis-server nginx supervisor python3.8-venv python3-pip gunicorn
cd /home/gitlab-runner/projects/ && python3 -m venv env
#source etc/bashrc || true
#source /home/gitlab-runner/projects/env/bin/activate && pip install -r courses/requirements.txt
cd /home/gitlab-runner/projects/courses && python3 manage.py collectstatic -y # collect static django in one place 

sudo chmod +x /home/gitlab-runner/projects/courses/bin/start_gunicorn.sh
sudo cp /home/gitlab-runner/projects/courses/confs_deploy/courses_gunicorn.conf /etc/supervisor/conf.d/
sudo service supervisor stop
sudo service supervisor start
#sudo cat /home/gitlab-runner/projects/courses/confs_deploy/nginx_conf_default > /etc/nginx/sites-enabled/default
sudo systemctl restart nginx.service
sudo chown -R gitlab-runner:gitlab-runner /home/gitlab-runner/projects
#chown gitlab-runner:gitlab-runner /home/gitlab-runner/projects/courses/db.sqlite3


# install postgresql
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - ; \
RELEASE=$(lsb_release -cs) ; \
echo "deb http://apt.postgresql.org/pub/repos/apt/ ${RELEASE}"-pgdg main | sudo tee  /etc/apt/sources.list.d/pgdg.list ; \
sudo apt update ; \
sudo apt -y install postgresql-11 ; \

# change pass user postgres
# sudo passwd postgres
# su - postgres
# export PATH=$PATH:/usr/lib/postgresql/11/bin
# createdb [MASKED] --username postgres
# create user [MASKED] with password '[MASKED]';
# ALTER USER [MASKED] CREATEDB;
# grant all privileges on database [MASKED_DB] to [MASKED_DB_user];
#GRANT ALL ON ALL TABLES IN SCHEMA public to [MASKED_DB_user];
#GRANT ALL ON ALL SEQUENCES IN SCHEMA public to [MASKED_DB_user];
#GRANT ALL ON ALL FUNCTIONS IN SCHEMA public to [MASKED_DB_user];
#CREATE EXTENSION pg_trgm;
#ALTER EXTENSION pg_trgm SET SCHEMA public;
#UPDATE pg_opclass SET opcdefault = true WHERE opcname='gin_trgm_ops';
#python manage.py makemigrations
#python manage.py migrate
#python manage.py createsuperuser [MASKED]]