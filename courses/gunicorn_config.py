command = '/home/gitlab-runner/projects/env/bin/gunicorn'
pythonpath = '/home/gitlab-runner/projects/courses/'
bind = '127.0.0.1:8001'
workers = 3
user = 'gitlab-runner'
limit_request_fields = 32000
limit_request_field_size = 0
raw_env = 'DJANGO_SETTINGS_MODULE=base.settings'
