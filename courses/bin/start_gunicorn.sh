#!/bin/bash
source /home/gitlab-runner/projects/env/bin
exec gunicorn -c "/home/gitlab-runner/projects/courses/gunicorn_config.py" base.wsgi
